/*
 *  rhwebimport
 *
 *  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
 *  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
 *
 *  This file is part of rhwebimport.
 *
 *  rhwebimport is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhwebimport is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var clock = null;
var auth = null;
var router = null;
var importer = null;
var rdxport = null;

jQuery.event.props.push('dataTransfer');

$(document).ready(function() {
  clock = new Clock();
  clock.start();
  auth = new Rdxport.Auth();
  router = new Rdxport.Router(auth);
  router.route();
});

Rdxport.Router = function(auth) {
  this.auth = auth;
  this.authView = new Rdxport.AuthView(this.auth);
  this.activeRoute = null;
};

Rdxport.Router.prototype.route = function(page, subpage) {
  if (!this.auth.isLoggedIn()) {
    this.login();
    return;
  }
  if (importer && importer.isUploading()) {
    alert('Achtung: Es laufen noch imports.');
    return;
  }

  if (!importer) {
    importer = new Rdxport.Importer(this.auth.username, this.auth.token, 'wss://' + window.location.host + '/rhimportd');
    window.onbeforeunload = function(event) {
      if (importer.isUploading()) {
        return 'Achtung: Es laufen noch imports.';
      }
    };
  }
  importer.closeAllUploads(true);
  if (!rdxport) {
    rdxport = new Rdxport.Rdxport(this.auth.username, this.auth.token, '/rd-bin/rdxport.cgi');
    rdxport.setListDropboxesEndpoint('/rh-bin/listdropboxes.cgi');
    rdxport.setCutEvergreenEndpoint('/rh-bin/cutevergreen.cgi');
    rdxport.setMusicgridEndpoint('/rh-bin/musicgrid.cgi');
  }

  /*$(document).ajaxError(function(event, jqXHR, settings, thrownError) {
    //todo: add errors
  });*/

  var href = ['', ''];
  if (!page && !subpage) {
    href = locationHrefValue();
    page = href[1];
    subpage = href[2];
  }

  shows_cleanup();
  jingles_cleanup();
  musicpools_cleanup();
  musicgrid_cleanup();

  this.authView.renderLoggedIn();
  $('.navbar-nav li').removeClass('active');
  $('.app-tab').hide();
  $('.container').removeClass('fullWidth');

  var self = this;
  $('.navbar-nav li a').off().on('click', function(event) {
    event.preventDefault();
    var href = $(this).attr('href').split('/');
    self.route(href[1], href[2]);
  });
  $(window).off('popstate').on('popstate', function(event) {
    var href = locationHrefValue();
    self.route(href[1], href[2]);
  });

  switch (page) {
    default :
      page = 'shows';
      // fallthrough
    case 'shows':
      this.shows(subpage);
      break;
    case 'jingles':
      this.jingles();
      break;
    case 'musicpools':
      this.musicpools(subpage);
      break;
    case 'musicgrid':
      this.musicgrid();
      break;
    case 'logout':
      this.logout();
      break;
  }
  this.activeRoute = page;

  href = locationHrefValue();
  if (href[1] !== page && page !== 'logout' && (!href[2] || href[2] !== subpage)) {
    var url = '/' + page + '/';
    if (subpage) {
      url += subpage + '/';
    }
    history.pushState(null, null, url);
  }
};

Rdxport.Router.prototype.login = function() {
  this.authView.renderLoginForm();
};

Rdxport.Router.prototype.logout = function() {
  if (importer && importer.isUploading()) {
    alert('Achtung: Es laufen noch imports.');
    return;
  }

  shows_cleanup();
  jingles_cleanup();
  musicpools_cleanup();
  musicgrid_cleanup();
  this.auth.cleanup();

  importer.closeAllUploads(true);
  importer = null;
  rdxport = null;

  this.login();
};

Rdxport.Router.prototype.shows = function(subpage) {
  $('#app-shows').show();
  $('#nav-btn-shows').addClass('active');
  shows_init(subpage);
};

Rdxport.Router.prototype.jingles = function() {
  $('#app-jingles').show();
  $('#nav-btn-jingles').addClass('active');
  jingles_init();
};

Rdxport.Router.prototype.musicpools = function(subpage) {
  $('#app-musicpools').show();
  $('#nav-btn-musicpools').addClass('active');
  musicpools_init(subpage);
};

Rdxport.Router.prototype.musicgrid = function() {
  $('.container').addClass('fullWidth');
  $('#app-musicgrid').show();
  $('#nav-btn-musicgrid').addClass('active');
  musicgrid_init();
};
