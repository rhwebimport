/*
 *  rhwebimport
 *
 *  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
 *  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
 *
 *  This file is part of rhwebimport.
 *
 *  rhwebimport is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhwebimport is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var Rdxport = Rdxport || {};

var jingleGroupListView = null;

function jingles_init() {
  var groupList = new Rdxport.GroupList();
  jingleGroupListView = new Rdxport.JingleGroupListView(groupList);
}

function jingles_cleanup() {
  if (jingleGroupListView) {
    jingleGroupListView.destroy();
    jingleGroupListView = null;
  }
}

Rdxport.JingleGroupListView = function(model) {
  this.model = model;

  this.jingleGroupViews = [];

  $('#app-jingles .groups').empty();

  var self = this;
  $(this.model).on('update', function() {
    $(self.model.groups).each(function(index, group) {
      self.jingleGroupViews.push(new Rdxport.JingleGroupView(group));
    });
  });
  this.model.fetch('jingle');
};

Rdxport.JingleGroupListView.prototype.destroy = function() {
  $(this.jingleGroupViews).each(function(index, groupView) {
    groupView.destroy();
  });
  this.jingleGroupViews = [];
};

Rdxport.JingleGroup = function(groupName, description, lowcart, highcart, normlevel, trimlevel, title) {
  if (arguments.length = 1) {
    Rdxport.Group.call(this, groupName);
    this.title = $('jingle-title', this.xml).text();
  } else {
    Rdxport.Group.call(this, groupName, description, lowcart, highcart, normlevel, trimlevel);
    this.title = title;
  }
};
Rdxport.JingleGroup.prototype = Object.create(Rdxport.Group.prototype);
Rdxport.JingleGroup.prototype.constructor = Rdxport.JingleGroup;

Rdxport.JingleGroupView = function(model) {
  this.model = model;

  this.cartView = null;

  this.$el = null;

  this.render();

  var self = this;
  $(this.model).on('update', function() {
    importer.syncUploads('jingles', self, function() {
      $('table > tbody', self.$el).empty();

      self.model.cart = self.model.carts[0];
      self.cartView = new Rdxport.JingleCartView(self.model.cart, self);
    });
  });

  this.model.fetchCarts();
};

Rdxport.JingleGroupView.prototype.render = function() {
  var self = this;

  this.$el = $('#hiddenTemplates .jingleGroupTemplate').clone().removeClass('jingleGroupTemplate');
  this.$el.appendTo('#app-jingles .groups');

  $('h2', this.$el).text(this.model.title);
  $('table tbody tr', this.$el).remove();

  $('.uploadButton', this.$el).on('click', function() {
    importer.openModal(self.model, self, self.model.carts[0].number, false, true);
  });
};

Rdxport.JingleGroupView.prototype.destroy = function() {
  $('table > tbody', this.$el).empty();
};

Rdxport.JingleGroupView.prototype.uploadProgress = function(upload) {
  if (!upload.uploadId) {
    return;
  }
  var $cut = $('tr[data-upload-id="' + upload.uploadId + '"]').first();
  if (!$cut.hasClass('uploading')) {
    var $progressBar = $('.progressBarTemplate.jingles').clone().removeClass('progressBarTemplate');
    $cut.html($progressBar.html());

    $('button', $cut).on('click', function() {
      upload.cancel();
    });

    $cut.addClass('uploading');
  }
  if (!upload.isNew && upload.title) {
    $('.file-name', $cut).text(upload.title);
  }
  if (upload.cartNumber && upload.cutNumber) {
    $('.cut-number', $cut).text(Rdxport.JingleCutView.createName(upload.cartNumber, upload.cutNumber));
  }

  updateProgressBar($cut, upload);
};


Rdxport.JingleGroupView.prototype.uploadError = function(upload) {
  Rdxport.JingleGroupView.uploadError(upload);
};

Rdxport.JingleGroupView.uploadError = function(upload) {
  var reason = $('<span>').addClass('label').addClass('label-danger').text(upload.errorStatus).after($('<b>').text(' ' + upload.errorString));

  var dismiss_button = '<button class="btn btn-info btn-xs">' +
    '<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Ok</button>';

  var status = $('<span class="label"></span>')
    .addClass('label-danger').html('<span class="glyphicon glyphicon-fire"></span>');

  var $errorRow = $('<tr>')
    .attr('data-upload-id', upload.uploadId)
    .append($('<td>').html(status))
    .append($('<td>').addClass('cut-number').text('...'))
    .append($('<td>').addClass('file-name'))
    .append($('<td>').append($('<b>').text('Import Fehler')))
    .append($('<td colspan="1">').append(reason))
    .append($('<td>').css('text-align', 'center').append(dismiss_button));

  if (upload.cutNumber) {
    $('.cut-number', $errorRow).text(upload.cutNumber);
  }
  if (upload.title) {
    $('.file-name', $errorRow).text(upload.title);
  }

  $('button', $errorRow).on('click', function() {
    upload.close();
  });

  var $cut = $('tr[data-upload-id="' + upload.uploadId + '"]').first();
  $cut.replaceWith($errorRow);
};

Rdxport.JingleCartView = function(model, groupView) {
  this.model = model;
  this.groupView = groupView;

  this.cutViews = [];

  var self = this;
  if (this.model) {
    $(this.model.cuts).sort(function(a, b) {
        if(a.isEvergreen == b.isEvergreen)
            return 0;
        if(a.isEvergreen)
            return 1;
        return -1;
    }).each(function(index, cut) {
      var cutView = new Rdxport.JingleCutView(cut);
      self.cutViews.push(cutView);

      $('table > tbody', self.groupView.$el).append(cutView.$el);
    });
    var uploads = importer.getUploadsByCartNumber(this.model.number);
    $(uploads).each(function(index, upload) {
      var $el = Rdxport.JingleCutView.renderUploading(upload);
      var $cut = $('#' + Rdxport.JingleCutView.createId(upload.cartNumber, upload.cutNumber));

      if (upload.cartNumber && upload.cutNumber && $cut.length > 0) {
        $cut.replaceWith($el);
      } else {
        $('table > tbody', self.groupView.$el).append($el);
      }
      if (upload.error) {
        Rdxport.JingleGroupView.uploadError(upload);
      }
    });
  }
};

Rdxport.JingleCutView = function(model) {
  this.model = model;

  this.$spinner = null;
  this.$el = null;
  this.render();
};

Rdxport.JingleCutView.createId = function(cartNumber, cutNumber) {
  cutNumber = ('000' + cutNumber + '').slice(-3);
  return 'jingle-' + cartNumber + '-' + cutNumber;
};

Rdxport.JingleCutView.createName = function(cartNumber, cutNumber) {
  var cartNumber = ('000000' + cartNumber + '').slice(-6);
  var cutNumber = ('000' + cutNumber + '').slice(-3);
  return cartNumber + '_' + cutNumber;
};

Rdxport.JingleCutView.prototype.render = function() {
  var status = $('<span class="label"></span>');
  var moveButton = $('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-transfer"></span>&nbsp;&nbsp;Verschieben</button>');
  var activateButton;
  if (!this.model.isEvergreen) {
    activateButton = $('<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Deaktivieren</button>');
    status.addClass('label-success').html('<span class="glyphicon glyphicon-star"></span>');
  } else {
    activateButton = $('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Aktivieren</button>');
    status.addClass('label-default').html('<span class="glyphicon glyphicon-star-empty"></span>');
  }
  var deleteButton = $('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp;Löschen</button>');

  if (this.model.length == 0) {
    status
      .removeClass('label-success')
      .addClass('label-danger')
      .html('<span class="glyphicon glyphicon-fire"></span>');
  }

  var self = this;
  moveButton.on('click', function() {
    self.move();
  });
  activateButton.on('click', function() {
    self.toggleEvergreen();
  });
  deleteButton.on('click', function() {
    self.delete();
  });

  this.$spinner = $('#hiddenTemplates .spinnerTemplate').clone().removeClass('spinnerTemplate');

  this.$el = $('<tr>')
      .attr('id', 'jingle-' + this.model.cartNumber + '-' + this.model.number)
      .append($('<td>').append(status))
      .append($('<td>').text(this.model.name))
      .append($('<td>').text(this.model.description))
      .append($('<td>').text(format_durationms(this.model.length)))
      .append($('<td>').text(format_datetime(this.model.lastPlayDatetime)))
      .append(
          $('<td>').addClass('text-center')
              .append(moveButton)
              .append(activateButton)
              .append(deleteButton)
      );
};

Rdxport.JingleCutView.renderUploading = function(upload) {
  var $progressBar = $('.progressBarTemplate.jingles').clone().removeClass('progressBarTemplate');
  if (upload.isNew) {
    var spinner = $('#hiddenTemplates .spinnerTemplate').clone().removeClass('spinnerTemplate');
    $('.file-name', $progressBar).html(spinner);
  } else {
    $('.file-name', $progressBar).text(upload.title);
  }

  var $el = $('<tr>')
    .html($progressBar.html())
    .addClass('uploading')
    .attr('data-upload-id', upload.uploadId);

  $('button', $el).on('click', function() {
    upload.cancel();
  });

  if (upload.cartNumber && upload.cutNumber) {
    $el.attr('id', Rdxport.JingleCutView.createId(upload.cartNumber, upload.cutNumber))
  }

  updateProgressBar($el, upload);
  return $el;
};

Rdxport.JingleCutView.prototype.move = function() {
  this.$el.find('td:first').html(this.$spinner);
  var self = this;
  var destinationCart = this.model.cartNumber;

  // todo: make this work for multiple groups
  if (jingleGroupListView.model.groups.length === 2) {
    $(jingleGroupListView.model.groups).each(function(index, group) {
      if (self.model.cartNumber !== group.cart.number) {
        destinationCart = group.cart;
      }
    });
  } else {
    return;
  }

  // todo: fix
  rdxport.moveCut(this.model.cartNumber, this.model.number, destinationCart.number, function() {
    self.model.cart.group.fetchCarts();
    destinationCart.group.fetchCarts();
  });
};

Rdxport.JingleCutView.prototype.toggleEvergreen = function() {
  this.$el.find('td:first').html(this.$spinner);
  var newvalue = 1;
  if(this.model.isEvergreen) {
    newvalue = 0;
  }
  var self = this;
  rdxport.setCutEvergreen(this.model.cartNumber, this.model.number, newvalue, function() {
    self.model.cart.group.fetchCarts();
  });
};

Rdxport.JingleCutView.prototype.delete = function() {
  this.$el.find('td:first').html(this.$spinner);
  var self = this;
  rdxport.removeCut(this.model.cartNumber, this.model.number, function() {
    self.model.cart.removeCut(self.model);
    self.$el.remove();
  });
};
