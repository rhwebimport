/*
 *  rhwebimport
 *
 *  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
 *  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
 *
 *  This file is part of rhwebimport.
 *
 *  rhwebimport is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhwebimport is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var Rdxport = Rdxport || {};

var musicpoolsView = null;

function musicpools_init(subpage) {
  var musicpools = new Rdxport.GroupList();
  musicpoolsView = new Rdxport.MusicpoolsView(musicpools, subpage);
}

function musicpools_cleanup() {
  musicpoolsView = null;
}

Rdxport.MusicpoolsView = function(model, subpage) {
  this.model = model;

  this.musicpoolViews = [];
  this.currentPoolId = null;

  this.setCurrentPoolId(subpage);

  var self = this;
  $(this.model).on('update', function() {
    $(self.model.groups).each(function(index, musicpool) {
      var musicpoolView = new Rdxport.MusicpoolView(musicpool);
      self.musicpoolViews.push(musicpoolView);
    });
    self.updateSelector();
  });
  this.model.fetch('musicpool');
};

Rdxport.MusicpoolsView.prototype.setCurrentPoolId = function(currentPoolId) {
  if (!currentPoolId) {
    return;
  }
  if (this.currentPoolId !== currentPoolId) {
    if (this.currentPoolId) {
      history.pushState(null, null, '/musicpools/' + currentPoolId + '/');
    } else {
      history.replaceState(null, null, '/musicpools/' + currentPoolId + '/');
    }
  }
  this.currentPoolId = currentPoolId;
  sessionStorage.setItem('currentPoolId', this.currentPoolId);
};

Rdxport.MusicpoolsView.prototype.getCurrentPoolView = function() {
  if (this.model.groups.length === 0) {
    return null;
  }
  if (this.musicpoolViews.length === 0) {
    return null;
  }
  if (this.currentPoolId === null) {
    this.setCurrentPoolId(this.model.groups[0].clock);
  }
  var self = this;
  var musicpoolViewFound = null;
  $(this.musicpoolViews).each(function(index, musicpoolView) {
    if (musicpoolView.model.clock === self.currentPoolId) {
      musicpoolViewFound = musicpoolView;
      return true;
    }
  });
  return musicpoolViewFound;
};

Rdxport.MusicpoolsView.prototype.updateSelector = function() {
  var self = this;
  var $musicpoolSelector = $('#musicpool-selector');

  $musicpoolSelector.off();
  $('li', $musicpoolSelector).remove();

  $(this.model.groups).sort(function(a, b) {
    var atitle = a.title.toLowerCase();
    var btitle = b.title.toLowerCase();
    if(atitle == btitle) {
      if(a.clock > b.clock) return 1;
      if(a.clock < b.clock) return -1;
      return 0;
    }
    if(atitle > btitle) return 1;
    return -1;
  }).each(function(index, musicpool) {
    var name = musicpool.clock + ' | <strong>' + musicpool.title + '</strong>';
    var link = $('<a>').attr('href', '#').html(name).click(function() {
      self.setCurrentPoolId(musicpool.clock);
      self.getCurrentPoolView().model.fetchCarts();
    });
    $musicpoolSelector.append($('<li>').append(link));
  });
  if($musicpoolSelector.children().length == 0) {
    $musicpoolSelector.append($('<li>').append($('<a>').text('Keinen Musikpool gefunden!')));
  }

  // todo: maybe integrate this into setCurrentShowId?
  if (!this.currentPoolId) {
    var currentPoolId = sessionStorage.getItem('currentPoolId');
    if (currentPoolId) {
      this.setCurrentPoolId(currentPoolId);
    } else {
      this.setCurrentPoolId(this.model.groups[0].id);
    }
  }

  this.getCurrentPoolView().model.fetchCarts();
};

Rdxport.Musicpool = function(groupName, description, lowcart, highcart, normlevel, trimlevel, title, clock) {
  if (arguments.length === 1) {
    Rdxport.Group.call(this, groupName);
    this.title = $('musicpool-title', this.xml).text();
    this.clock = $('musicpool-clock', this.xml).text();
  } else {
    Rdxport.Group.call(this, groupName, description, lowcart, highcart, normlevel, trimlevel);
    this.title = title;
    this.clock = clock;
  }
};
Rdxport.Musicpool.prototype = Object.create(Rdxport.Group.prototype);
Rdxport.Musicpool.prototype.constructor = Rdxport.Musicpool;

Rdxport.MusicpoolView = function(model) {
  this.model = model;
  this.cartViews = [];

  this.$el = null;

  var self = this;
  $(this.model).on('update', function() {
    importer.syncUploads('musicpools', self, function() {
      self.render();
    });
  });
};

Rdxport.MusicpoolView.prototype.render = function() {
  var self = this;

  this.$el = $('#hiddenTemplates .musicpoolTemplate').clone().removeClass('musicpoolTemplate');
  $('#app-musicpools .musicpoolContainer').html(this.$el);

  $('#musicpool-title').text(this.model.title);
  $('#musicpool-cnt').text(this.model.carts.length);
  $('table tbody tr', this.$el).remove();

  this.cartViews = [];
  $(this.model.carts).each(function(index, cart) {
    var cartView = new Rdxport.MusicpoolCartView(cart);
    self.cartViews.push(cartView);

    $('table > tbody', self.$el).append(cartView.$el);
  });
  var uploads = importer.getUploadsByGroupName(this.model.name);
  $(uploads).each(function(index, upload) {
    var $el = Rdxport.MusicpoolCartView.renderUploading(upload);
    var $cart = $('#musicpool-' + upload.cartNumber, self.$el);
    if (upload.cartNumber && $cart.length > 0) {
      $cart.replaceWith($el);
    } else {
      $('table > tbody', self.$el).append($el);
    }
    if (upload.error) {
      self.uploadError(upload);
    }
  });


  $('.uploadButton', this.$el).on('click', function() {
    importer.openModal(self.model, self, null, true, true);
  });
};

Rdxport.MusicpoolView.prototype.uploadProgress = function(upload) {
  if (!upload.uploadId) {
    return;
  }
  var $cart = $('tr[data-upload-id="' + upload.uploadId + '"]').first();
  if (!$cart.hasClass('uploading')) {
    var $progressBar = $('.progressBarTemplate.musicpools').clone().removeClass('progressBarTemplate');
    $cart.html($progressBar.html());

    $('button', $cart).on('click', function() {
      upload.cancel();
    });

    $cart.addClass('uploading');
  }
  if (!upload.isNew && upload.title) {
    $('.file-name', $cart).text(upload.title);
  }
  if (upload.cartNumber) {
    $('.cart-number', $cart).text(upload.cartNumber);
  }

  updateProgressBar($cart, upload);
};

Rdxport.MusicpoolView.prototype.uploadError = function(upload) {
  var reason = $('<span>').addClass('label').addClass('label-danger').text(upload.errorStatus).after($('<b>').text(' ' + upload.errorString));

  var dismiss_button = '<button class="btn btn-info btn-xs">' +
    '<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Ok</button>';

  var $errorRow = $('<tr>')
    .attr('data-upload-id', upload.uploadId)
    .append($('<td>').addClass('.cart-number').text('...'))
    .append($('<td>').addClass('file-name'))
    .append($('<td>').append($('<b>').text('Import Fehler')))
    .append($('<td colspan="5">').append(reason))
    .append($('<td>').css('text-align', 'center').append(dismiss_button));

  if (upload.cartNumber) {
    $('.cart-number', $errorRow).text(upload.cartNumber);
  }
  if (upload.title) {
    $('.file-name', $errorRow).text(upload.title);
  }

  $('button', $errorRow).on('click', function() {
    upload.close();
  });

  var $cart = $('tr[data-upload-id="' + upload.uploadId + '"]').first();
  $cart.replaceWith($errorRow);
};

Rdxport.MusicpoolCartView = function(model) {
  this.model = model;

  this.$spinner = null;
  this.$el = null;

  this.render();
};

Rdxport.MusicpoolCartView.prototype.render = function() {
  var length = '-';
  var imported = '-';
  var playcnt = '-';
  var lastplayed = '-';

  if (this.model.cuts.length > 0) {
    var cut = this.model.cuts[0];

    length = format_durationms(Number(cut.length));
    if (!isNaN(cut.originDatetime)) {
      imported = format_datetime(cut.originDatetime);
    }
    playcnt = cut.playCounter;
    if (!isNaN(cut.lastPlayDatetime)) {
      lastplayed = format_datetime(cut.lastPlayDatetime);
    }
  }

  var deleteButton = $('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp;Löschen</button>');

  var self = this;
  deleteButton.on('click', function() {
    self.delete();
  });

  this.$spinner = $('#hiddenTemplates .spinnerTemplate').clone().removeClass('spinnerTemplate');

  this.$el = $('<tr>')
    .attr('id', 'musicpool-' + this.model.number)
    .append($('<td>').text(this.model.number))
    .append($('<td>').text(this.model.title))
    .append($('<td>').text(this.model.artist))
    .append($('<td>').text(this.model.album))
    .append($('<td>').text(length))
    .append($('<td>').text(imported))
    .append($('<td>').text(playcnt))
    .append($('<td>').text(lastplayed))
    .append($('<td>').addClass('text-center').append(deleteButton));
};

Rdxport.MusicpoolCartView.renderUploading = function(upload) {
  var $progressBar = $('.progressBarTemplate.musicpools').clone().removeClass('progressBarTemplate');
  if (upload.isNew) {
    var spinner = $('#hiddenTemplates .spinnerTemplate').clone().removeClass('spinnerTemplate');
    $('.file-name', $progressBar).html(spinner);
  } else {
    $('.file-name', $progressBar).text(upload.title);
  }

  var $el = $('<tr>')
    .html($progressBar.html())
    .addClass('uploading')
    .attr('data-upload-id', upload.uploadId);

  if (upload.cartNumber) {
    $el.attr('id', 'musicpool-' + upload.cartNumber);
  }

  $('button', $el).on('click', function() {
    upload.cancel();
  });

  updateProgressBar($el, upload);
  return $el;
};

Rdxport.MusicpoolCartView.prototype.delete = function() {
  this.$el.find('td:last').html(this.$spinner);

  var self = this;
  rdxport.removeCart(this.model.number, function() {
    self.model.group.removeCart(self.model);
    self.$el.remove();
  });
};
