/*
 *  rhwebimport
 *
 *  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
 *  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
 *
 *  This file is part of rhwebimport.
 *
 *  rhwebimport is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhwebimport is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var Rdxport = Rdxport || {};

Rdxport.Rdxport.authLogin = function(authEndpoint, username, password, success) {
  return $.ajax(authEndpoint, {
    username: username,
    password: password,
    success: success,
    dataType: 'json',
    cache: false
  });
};

Rdxport.Rdxport.prototype.setListDropboxesEndpoint = function(listDropboxesEndpoint) {
  this.listDropboxesEndpoint = listDropboxesEndpoint;
};

Rdxport.Rdxport.prototype.setCutEvergreenEndpoint = function(cutEvergreenEndpoint) {
  this.cutEvergreenEndpoint = cutEvergreenEndpoint;
};

Rdxport.Rdxport.prototype.setMusicgridEndpoint = function(musicgridEndpoint) {
  this.musicgridEndpoint = musicgridEndpoint;
};

Rdxport.Rdxport.prototype.listDropboxes = function(type, success) {
  var command = {
    LOGIN_NAME: this.username,
    PASSWORD: this.token
  };
  if (type !== null) {
    command.TYPE = type;
  }
  return $.post(this.listDropboxesEndpoint, command, success, 'xml');
};

// see: https://github.com/ElvishArtisan/rivendell/pull/100
Rdxport.Rdxport.prototype.addAndEditCut = function(cartNumber, options, success) {
  options.COMMAND = 10;
  options.LOGIN_NAME = this.username;
  options.PASSWORD = this.token;
  options.CART_NUMBER = cartNumber;
  return $.post(this.endpoint, options, success, "xml");
};

Rdxport.Rdxport.prototype.copyCut = function(sourceCartNumber, sourceCutNumber,
                                               destinationCartNumber,
                                               success) {
  var self = this;
  var returnJqXHR = null;
  this.addCut(destinationCartNumber, function(data, textStatus, jqXHR) {
    var command = {
      COMMAND: 24,
      LOGIN_NAME: self.username,
      PASSWORD: self.token,
      SOURCE_CART_NUMBER: sourceCartNumber,
      SOURCE_CUT_NUMBER: sourceCutNumber,
      DESTINATION_CART_NUMBER: destinationCartNumber,
      DESTINATION_CUT_NUMBER: $('cutAdd cut cutNumber', data).text()
    };
    returnJqXHR = $.post(self.endpoint, command, success, 'xml')
      .fail(function() {
        self.removeCut(destinationCartNumber, command.DESTINATION_CUT_NUMBER, null);
      });
  });
  return returnJqXHR;
};

Rdxport.Rdxport.prototype.addAndEditCart = function(groupName, type, cartNumber, options, success) {
  options.COMMAND = 12;
  options.LOGIN_NAME = this.username;
  options.PASSWORD = this.token;
  options.GROUP_NAME = groupName;
  options.TYPE = type;
  if (cartNumber !== null) {
    options.CART_NUMBER = cartNumber;
  }
  return $.post(this.endpoint, options, success, 'xml');
};

Rdxport.Rdxport.prototype.setCutEvergreen = function(cartNumber, cutNumber, value, success) {
  var command = {
    LOGIN_NAME: this.username,
    PASSWORD: this.token,
    CART_NUMBER: cartNumber,
    CUT_NUMBER: cutNumber,
    VALUE: value
  };
  return $.post(this.cutEvergreenEndpoint, command, success, "xml");
};

Rdxport.Rdxport.prototype.getMusicgrid = function(success) {
  var command = {
    LOGIN_NAME: this.username,
    PASSWORD: this.token
  };
  return $.get(this.musicgridEndpoint, command, success, "xml");
};

Rdxport.Rdxport.prototype.setMusicgrid = function(dow, hour, name, success) {
  var command = {
    LOGIN_NAME: this.username,
    PASSWORD: this.token,
    DOW: dow,
    HOUR: hour,
    NAME: name
  };
  return $.post(this.musicgridEndpoint, command, success, "xml");
};

Rdxport.GroupList.prototype.fetch = function(type) {
  this.groups = [];

  var self = this;
  rdxport.listDropboxes(type, function(groupsXml, status, req) {
    var dbs = $('dropboxList', groupsXml).children();
    dbs.each(function(index, groupXml) {
      var group = null;
      switch ($('type', groupXml).text()) {
        case 'show':
          group = new Rdxport.Show(groupXml);
          break;
        case 'jingle':
          group = new Rdxport.JingleGroup(groupXml);
          break;
        case 'musicpool':
          group = new Rdxport.Musicpool(groupXml);
          break;
        default:
          group = new Rdxport.Group(groupXml);
          break;
      }

      if (group !== null) {
        self.groups.push(group);
      }
    });

    $(self).trigger('update');
  });
};

Rdxport.Musicgrid = function() {
  this.clocks = [];
};

Rdxport.Musicgrid.prototype.fetch = function() {
  this.clocks = [];

  var self = this;
  rdxport.getMusicgrid(function(gridXml, status, req) {
    var dbs = $('grid', gridXml).children();
    dbs.each(function(index, clockXml) {
      self.clocks.push(new Rdxport.MusicgridClock(clockXml));
    });
    $(self).trigger('update');
  });
};

Rdxport.MusicgridClock = function(name, color, title, dow, hour) {
  this.xml = null;

  if (arguments.length === 1) {
    this.xml = arguments[0];
    this.name = $('name', this.xml).text();
    this.color = $('color', this.xml).text();
    this.title = $('title', this.xml).text();
    this.dow = $(this.xml).attr('dow');
    this.hour = $(this.xml).attr('hour');
  } else {
    this.name = name;
    this.color = color;
    this.title = title;
    this.dow = dow;
    this.hour = hour;
  }
};
