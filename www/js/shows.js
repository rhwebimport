/*
 *  rhwebimport
 *
 *  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
 *  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
 *
 *  This file is part of rhwebimport.
 *
 *  rhwebimport is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhwebimport is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var Rdxport = Rdxport || {};

var showListView = null;

function shows_init(subpage) {
  var showList = new Rdxport.GroupList();
  showListView = new Rdxport.ShowListView(showList, subpage);

  drawClock('Do, 1.1.1970', '00:00:00', 0);
  clock_add_callback(drawClock);
}

function shows_cleanup() {
  $('#show-carts tbody').find('tr').remove();
}

Rdxport.ShowListView = function(model, subpage) {
  this.model = model;

  this.showViews = [];
  this.currentShowId = null;

  this.setCurrentShowId(subpage);

  var self = this;
  $(this.model).off('update').on('update', function() {
    $(self.model.groups).each(function(index, show) {
      var showView = new Rdxport.ShowView(show);
      self.showViews.push(showView);
    });
    self.updateSelector();
  });
  this.model.fetch('show');
};

Rdxport.ShowListView.prototype.setCurrentShowId = function(currentShowId) {
  if (!currentShowId) {
    return;
  }
  if (this.currentShowId !== currentShowId) {
    if (this.currentShowId) {
      history.pushState(null, null, '/shows/' + currentShowId + '/');
    } else {
      history.replaceState(null, null, '/shows/' + currentShowId + '/');
    }
  }
  this.currentShowId = currentShowId;
  sessionStorage.setItem('currentShowId', this.currentShowId);
};

Rdxport.ShowListView.prototype.getCurrentShowView = function() {
  if (this.model.groups.length === 0) {
    return null;
  }
  if (this.showViews.length === 0) {
    return null;
  }
  var self = this;
  var showViewFound = null;
  $(this.showViews).each(function(index, showView) {
    if (showView.model.id === parseInt(self.currentShowId)) {
      showViewFound = showView;
      return true;
    }
  });
  return showViewFound;
};

Rdxport.ShowListView.prototype.updateSelector = function() {
  var self = this;
  var $showSelector = $('#show-selector');

  $showSelector.off();
  $('li', $showSelector).remove();

  $(this.model.groups).sort(function(a, b) {
    var atitle = a.title.toLowerCase();
    if(a.type == "r") {
      atitle += " (Wiederholung)";
    } else if(a.type == 's') {
      atitle += " (Sondersendung)";
    }
    var btitle = b.title.toLowerCase();
    if(b.type == "r") {
      btitle += " (Wiederholung)";
    } else if(b.type == 's') {
      btitle += " (Sondersendung)";
    }
    if(atitle == btitle) {
      if(b.rhythm == a.rhythm)  {
        if(a.dayofweek == b.dayofweek) {
          if(a.starttime == b.starttime) {
            return a.showid - b.showid;
          }
          if(a.starttime > b.starttime) return 1;
          return -1;
        }
        var adow = a.dayofweek == 0 ? 7 : a.dayofweek;
        var bdow = b.dayofweek == 0 ? 7 : b.dayofweek;
        return adow - bdow;
      }
      return parseInt(b.rhythm) - parseInt(a.rhythm);
    }
    if(atitle > btitle) return 1;
    return -1;
  }).each(function(index, show) {
    var title = show.title;
    if(show.type == "r") {
      title += " (Wiederholung)";
    } else if(show.type == 's') {
      title += " (Sondersendung)";
    }
    var name = show.id + ' | <strong>' + title + '</strong> (' + show.rhythm + ', ' + weekday[show.dayofweek] + ', ' + show.starttime + ', ' + (show.length == 0 ? '-' : show.length) + ' Min.)';
    var link = $('<a>').attr('href', '#').html(name).click(function() {
      self.setCurrentShowId(show.id);
      self.getCurrentShowView().model.fetchCarts();
    });
    $showSelector.append($('<li>').append(link));
  });
  if($showSelector.children().length == 0) {
    $showSelector.append($('<li>').append($('<a>').text('Keine Sendung gefunden!')));
  }

  // todo: maybe integrate this into setCurrentShowId?
  if (!this.currentShowId) {
    var currentShowId = sessionStorage.getItem('currentShowId');
    if (currentShowId) {
      this.setCurrentShowId(currentShowId);
    } else {
      this.setCurrentShowId(this.model.groups[0].id);
    }
  }

  this.getCurrentShowView().model.fetchCarts();
};

Rdxport.Show = function(groupName, description, lowcart, highcart, normlevel, trimlevel,
                          id, title, log, rhythm, dayofweek, starttime, length, type) {
  if (arguments.length = 1) {
    Rdxport.Group.call(this, groupName);
    this.id = parseInt($('show-id', this.xml).text());
    this.title = $('show-title', this.xml).text();
    this.log = $('show-log', this.xml).text();
    this.rhythm = $('show-rhythm', this.xml).text();
    this.dayofweek = parseInt($('show-dayofweek', this.xml).text());
    this.starttime = $('show-starttime', this.xml).text();
    this.length = parseInt($('show-length', this.xml).text());
    this.type = $('show-type', this.xml).text();
  } else {
    Rdxport.Group.call(this, groupName, description, lowcart, highcart, normlevel, trimlevel);
    this.id = id;
    this.title = title;
    this.log = log;
    this.rhythm = rhythm;
    this.dayofweek = dayofweek;
    this.starttime = starttime;
    this.length = length;
    this.type = type;
  }
  this.dayofweek = (this.dayofweek < 0 || this.dayofweek > 7) ? 7 : this.dayofweek;
  this.length = (this.length < 0) ? 0 : this.length;

  this.logs = [];
};
Rdxport.Show.prototype = Object.create(Rdxport.Group.prototype);
Rdxport.Show.prototype.constructor = Rdxport.Show;

Rdxport.Show.prototype.fetchCarts = function() {
  var self = this;

  var lcd = rdxport.listLog(this.log, function(logsXml, status, req) {
    self.logs = [];

    var loglines = $('logList', logsXml).children();
    loglines.each(function(index, logXml) {
      var log = Rdxport.Log.newFromXml(logXml, self);
      if(log.cartNumber >= self.lowcart &&
          log.cartNumber <= self.highcart) {
        self.logs.push(log);
      }
    });
  });

  var gcd = rdxport.listCarts({GROUP_NAME: this.groupName, INCLUDE_CUTS: 1}, function(cartsXml, status, req) {
    self.carts = [];
    self.cartsByNumber = {};

    var dbs = $('cartList', cartsXml).children();
    dbs.each(function(index, cartXml) {
      var cart = new Rdxport.Cart(cartXml, self);

      var cuts = $('cutList', cartXml).children();
      cuts.each(function(index, cut) {
        cart.cuts.push(new Rdxport.Cut(cut, cart));
      });

      self.addCart(cart);
    });
  });

  $.when(lcd, gcd).done(function(lcres, gcres) {
    if(lcres[1] == 'success' && gcres[1] == 'success') {
      self.$this.trigger('update');
    }
  });
};

Rdxport.ShowView = function(model) {
  this.model = model;
  this.cartViews = [];

  this.$el = null;
  var self = this;
  this.model.$this.off('update').on('update', function() {
    importer.syncUploads('shows', self, function() {
      self.render();
    });
  });
};

Rdxport.ShowView.prototype.render = function() {
  if(this.model.type == 'r') {
    $('#show-title').text(this.model.title + " (Wiederholung)");
  } else if(this.model.type == 's') {
    $('#show-title').text(this.model.title + " (Sondersendung)");
  } else {
    $('#show-title').text(this.model.title);
  }
  $('#show-dow').text(weekday[this.model.dayofweek]);
  $('#show-starttime').text(this.model.starttime);
  $('#show-length').text((this.model.length == 0 ? '-' : this.model.length) + ' Min.');

  for(var w = 0; w < 4; w++) {
    if(this.model.rhythm.charAt(w) == '1') {
      var s = $('#show-rhythm-w' + (w+1)).attr('class', 'label');
      switch(w+1) {
        case 1: s.addClass('label-info'); break;
        case 2: s.addClass('label-warning'); break;
        case 3: s.addClass('label-success'); break;
        case 4: s.addClass('label-danger'); break;
      }
    } else {
      $('#show-rhythm-w' + (w+1)).attr('class', 'label label-disabled')
    }
  }

  var $tableBody = $('#app-shows table tbody', this.$el);
  $('tr', $tableBody).remove();

  this.cartViews = [];
  var self = this;
  $(self.model.logs).each(function(index, log) {
    var cart = self.model.getCartByNumber(log.cartNumber);
    var upload = importer.getUploadByCartNumber(log.cartNumber);
    if (upload) {
      $('#app-shows table > tbody', self.$el).append(Rdxport.ShowCartView.renderUploading(upload));
      if (upload.error) {
        self.uploadError(upload);
      }
    } else if (cart) {
      var cartView = new Rdxport.ShowCartView(cart, self);
      self.cartViews.push(cartView);
      cartView.render();
      $('#app-shows table > tbody', self.$el).append(cartView.$el);
    } else {
      $('#app-shows table > tbody', self.$el).append(Rdxport.ShowCartView.renderEmpty(self.model, self, log.cartNumber));
    }
  });
};

Rdxport.ShowView.prototype.uploadProgress = function(upload) {
  if (!upload.uploadId) {
    return;
  }
  var $cart = $('tr[data-upload-id="' + upload.uploadId + '"]', this.$el).first();
  if (!$cart.hasClass('uploading')) {
    var $progressBar = $('.progressBarTemplate.shows').clone().removeClass('progressBarTemplate');
    $cart.html($progressBar.html());

    $('button', $cart).off('click').on('click', function() {
      upload.cancel();
    });

    $cart.addClass('uploading');
  }
  if (!upload.isNew && upload.title) {
    $('.file-name', $cart).text(upload.title);
  }
  if (upload.cartNumber) {
    $('.cart-number', $cart).text(upload.cartNumber);
  }

  updateProgressBar($cart, upload);
};

Rdxport.ShowView.prototype.uploadError = function(upload) {
  var reason = $('<span>').addClass('label').addClass('label-danger').text(upload.errorStatus).after($('<b>').text(' ' + upload.errorString));

  var dismiss_button = '<button class="btn btn-info btn-xs">' +
    '<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Ok</button>';

  var $errorRow = $('<tr>')
      .attr('data-upload-id', upload.uploadId)
      .append($('<td>').addClass('.cart-number').text('...'))
      .append($('<td>').addClass('file-name'))
      .append($('<td>').append($('<b>').text('Import Fehler')))
      .append($('<td colspan="3">').append(reason))
      .append($('<td>').css('text-align', 'center').append(dismiss_button));

  if (upload.cartNumber) {
    $('.cart-number', $errorRow).text(upload.cartNumber);
  }
  if (upload.title) {
    $('.file-name', $errorRow).text(upload.title);
  }

  $('button', $errorRow).off('click').on('click', function() {
    upload.close();
  });

  var $cart = $('tr[data-upload-id="' + upload.uploadId + '"]', this.$el).first();
  $cart.replaceWith($errorRow);
};

Rdxport.ShowCartView = function(model, groupView) {
  this.model = model;
  this.groupView = groupView;

  this.$spinner = null;
  this.$el = $('<tr>');
};

Rdxport.ShowCartView.prototype.render = function() {
  var $deleteButton = $('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp;Löschen</button>');

  var self = this;
  $deleteButton.off('click').on('click', function() {
    self.delete();
  });

  this.$spinner = $('#hiddenTemplates .spinnerTemplate').clone().removeClass('spinnerTemplate');

  var cut = this.model.cuts[0];
  var length = '-';
  var originDatetime = '-';
  var playCounter = '-';
  var lastPlayDatetime = '-';
  if (cut) {
    length = format_durationms(Number(cut.length));
    originDatetime = format_datetime(cut.originDatetime);
    playCounter = cut.playCounter;
    lastPlayDatetime = format_datetime(cut.lastPlayDatetime);
  }
  this.$el = this.$el.empty()
    .attr('id', 'show-cart-' + this.model.number)
    .append($('<td>').text(this.model.number))
    .append($('<td>').text(this.model.title))
    .append($('<td>').text(length))
    .append($('<td>').text(originDatetime))
    .append($('<td>').text(playCounter))
    .append($('<td>').text(lastPlayDatetime))
    .append($('<td>').css('text-align', 'center').append($deleteButton));
};

Rdxport.ShowCartView.renderUploading = function(upload) {
  var $progressBar = $('.progressBarTemplate.shows').clone().removeClass('progressBarTemplate');
  if (upload.isNew) {
    var spinner = $('#hiddenTemplates .spinnerTemplate').clone().removeClass('spinnerTemplate');
    $('.file-name', $progressBar).html(spinner);
  } else {
    $('.file-name', $progressBar).text(upload.title);
  }
  $('.cart-number', $progressBar).text(upload.cartNumber);

  var $el = $('<tr>')
    .html($progressBar.html())
    .attr('id', 'show-cart-' + upload.cartNumber)
    .addClass('uploading')
    .attr('data-upload-id', upload.uploadId);

  $('button', $el).off('click').on('click', function() {
    upload.cancel();
  });

  updateProgressBar($el, upload);

  return $el;
};

Rdxport.ShowCartView.renderEmpty = function(group, groupView, cartNumber) {
  var $uploadButton = $('<button class="uploadButton btn btn-primary btn-xs"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;Importieren</button>');

  $uploadButton.off('click').on('click', function() {
    importer.openModal(group, groupView, cartNumber, true, false);
  });

  return $('<tr>')
          .attr('id', 'show-cart-' + cartNumber)
          .append($('<td>').text(cartNumber))
          .append($('<td>').text('-'))
          .append($('<td>').text('-'))
          .append($('<td>').text('-'))
          .append($('<td>').text('-'))
          .append($('<td>').text('-'))
          .append($('<td>').css('text-align', 'center').append($uploadButton));
};

Rdxport.ShowCartView.prototype.delete = function() {
  $('td:last', this.$el).html(this.$spinner);

  var self = this;
  rdxport.removeCart(this.model.number, function() {
    self.model.group.removeCart(self.model);
    var $empty = Rdxport.ShowCartView.renderEmpty(self.model.group, self.groupView, self.model.number);
    self.$el.replaceWith($empty);
  });
};
