/*
 *  rhwebimport
 *
 *  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
 *  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
 *
 *  This file is part of rhwebimport.
 *
 *  rhwebimport is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhwebimport is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var Rdxport = Rdxport || {};

var musicgridView = null;
var musicpoolModal = null;

function musicgrid_init() {
  var musicgrid = new Rdxport.Musicgrid();
  musicgridView = new Rdxport.MusicgridView(musicgrid);

  var musicpools = new Rdxport.GroupList();
  musicpoolModal = new Rdxport.MusicpoolModal(musicpools);
}

function musicgrid_cleanup() {
  musicpoolModal = null;

  if (musicgridView) {
    musicgridView.clean();
  }
  musicgridView = null;
}

Rdxport.MusicgridView = function(model) {
  this.model = model;

  this.$el = $('#app-musicgrid table');

  var self = this;
  $(this.model).on('update', function() {
    self.update();
  });
  this.model.fetch();
};

Rdxport.MusicgridView.prototype.clean = function() {
  $('tr td', this.$el)
      .empty()
      .removeClass('clock')
      .css('background-color', '')
      .css('color', '')
      .attr('title', null);
};

Rdxport.MusicgridView.prototype.update = function() {
  this.clean();

  $(this.model.clocks).each(function(index, clock) {
    var $td = $('tr[data-dow="' + clock.dow + '"] td[data-hour="' + clock.hour +'"]', this.$el);
    $td.addClass('clock');
    $td.text(clock.name);
    $td.attr('title', clock.title);
    $td.css('background-color', clock.color);
    if($td.isBackgroundDark()) {
      $td.css('color', '#ffffff');
    }

    // todo: move this css to stylesheet
    $td.css('text-align', 'center');

    $td.off().on('click', function() {
      musicpoolModal.selectClock(clock.dow, clock.hour, clock.name);
    });
  });

  $('tr td:not(.clock)', this.$el).off().on('click', function() {
    musicpoolModal.selectClock(
        $(this).parent().data('dow'),
        $(this).data('hour'),
        null
    );
  });
};

Rdxport.MusicpoolModal = function(model) {
  this.model = model;
};

Rdxport.MusicpoolModal.prototype.selectClock = function(dow, hour, clockName) {
  $('#musicpoolModal').modal({keyboard: true});

  var $modalBody = $('#musicpoolModal .modal-body');
  var $modalHeader = $('#musicpoolModal .modal-header');

  var hourstr = Number(hour).pad(2) + ':00-' + Number(Number(hour) + 1).pad(2) + ':00';
  $('h4', $modalHeader).text('Musikpool auswählen für: ' + weekday[dow] + ' ' + hourstr);
  $('tbody', $modalBody).empty();

  var self = this;
  $(this.model).off().on('update', function() {
    $('tbody tr', $modalBody).remove();
    $(self.model.groups).each(function(index, musicpool) {
      var $button = null;
      if (clockName === musicpool.clock) {
        $button = $('<button class="btn btn-primary" disabled="disabled">Selected</button>');
      } else {
        $button = $('<button class="btn btn-primary">Select</button>');
      }

      $button.on('click', function() {
        rdxport.setMusicgrid(dow, hour, musicpool.clock, function() {
          $('#musicpoolModal').modal('hide');
          musicgridView.model.fetch();
        });
      });

      var $tr = $('<tr>');
      $tr.append($('<td>').text(musicpool.clock));
      $tr.append($('<td>').text(musicpool.title));
      $tr.append($('<td>').html($button));

      $('tbody', $modalBody).append($tr);
    });
  });
  this.model.fetch('musicpool');
};
