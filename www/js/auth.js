/*
 *  rhwebimport
 *
 *  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
 *  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
 *
 *  This file is part of rhwebimport.
 *
 *  rhwebimport is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhwebimport is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var Rdxport = Rdxport || {};

Rdxport.Auth = function() {
  this.username = sessionStorage.getItem('auth_username');
  this.fullname = sessionStorage.getItem('auth_fullname');
  this.token = sessionStorage.getItem('auth_token');
};

Rdxport.Auth.prototype.isLoggedIn = function() {
  return this.username && this.fullname && this.token;
};

Rdxport.Auth.prototype.set = function(username, fullname, token) {
  this.username = username;
  this.fullname = fullname;
  this.token = token;

  sessionStorage.setItem('auth_username', this.username);
  sessionStorage.setItem('auth_fullname', this.fullname);
  sessionStorage.setItem('auth_token', this.token);
};

Rdxport.Auth.prototype.cleanup = function() {
  sessionStorage.removeItem('auth_username');
  sessionStorage.removeItem('auth_fullname');
  sessionStorage.removeItem('auth_token');

  this.username = null;
  this.fullname = null;
  this.token = null;
};

Rdxport.AuthView = function(model) {
  this.model = model;
};

Rdxport.AuthView.prototype.renderLoggedIn = function() {
  $('#loginbox').slideUp();
  $('#mainwindow').fadeIn();
  $('#username-field').text(this.model.fullname + ' (' + this.model.username + ')');

  $('button.logout').off().on('click', function() {
    router.route('logout');
  });
};

Rdxport.AuthView.prototype.renderLoginForm = function() {
  $('.alert').alert('close');
  $('#loginbox').slideDown();
  $('#mainwindow').fadeOut();
  $('#username-field').empty();

  var self = this;
  $('#loginform').on('submit', function(event) {
    event.preventDefault();
    Rdxport.Rdxport.authLogin(
      '/rh-bin/authtoken.json',
      $("#username").val(),
      $("#password").val(),
      function(data) {
        if (data.status == 'OK') {
          self.model.set(
            data.username,
            data.fullname,
            data.token
          );

          router.route();
        } else {
          alertbox.error('loginbox', "Fehler beim Login", data.errorstring);
          self.model.cleanup();
        }
      }
    ).fail(function(req, status, error) {
        var message = req.status + ': ' + error;
        if(req.status == 401) {
          message = "Benutzer und/oder Passwort sind falsch!";
        }
        alertbox.error('loginbox', "Fehler beim Login", message);
        $("#password").val('');
      });
  });
};
