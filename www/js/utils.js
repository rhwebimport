/*
 *  rhwebimport
 *
 *  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
 *  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
 *
 *  This file is part of rhwebimport.
 *
 *  rhwebimport is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhwebimport is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var Rdxport = Rdxport || {};

var alertbox = function() {};
alertbox.warning = function (dest, heading, message) {
  $('#' + dest + ' .alertbox').html('<div class="alert alert-warning"><a class="close" data-dismiss="alert" href="#">&times;</a><h4 class="alert-heading">' + heading + '</h4>' + message + '</div>');
};
alertbox.error = function (dest, heading, message) {
  $('#' + dest + ' .alertbox').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#">&times;</a><h4 class="alert-heading">' + heading + '</h4>' + message + '</div>');
};
alertbox.info = function (dest, heading, message) {
  $('#' + dest + ' .alertbox').html('<div class="alert alert-info"><a class="close" data-dismiss="alert" href="#">&times;</a><h4 class="alert-heading">' + heading + '</h4>' + message + '</div>');
};
alertbox.success = function (dest, heading, message) {
  $('#' + dest + ' .alertbox').html('<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a><h4 class="alert-heading">' + heading + '</h4>' + message + '</div>');
};

Number.prototype.pad = function(size) {
  var s = String(this);
  while (s.length < (size || 2)) {s = "0" + s;}
  return s;
};

var dateToArchiveUri = function(date) {
  return 'archiv://' + date.getFullYear().pad(4)
    + '/' + (date.getMonth() + 1).pad(2)
    + '/' + date.getDate().pad(2)
    + '/' + date.getHours().pad(2)
    + '/' + date.getMinutes().pad(2);
};

var weekday = new Array(7);
weekday[0] = 'Sonntag';
weekday[1] = 'Montag';
weekday[2] = 'Dienstag';
weekday[3] = 'Mittwoch';
weekday[4] = 'Donnerstag';
weekday[5] = 'Freitag';
weekday[6] = 'Samstag';
weekday[7] = '-';

var weekday_short = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', '-'];

function format_datetime(d) {
  if(Object.prototype.toString.call(d) !== '[object Date]') {
    return '-';
  }
  if (d.toString() === 'Invalid Date') {
    return '-';
  }
  var datetimestr = weekday_short[d.getDay()];
  datetimestr += ' ' + Number(d.getDate()).pad(2);
  datetimestr += '.' + Number(d.getMonth() + 1).pad(2);
  datetimestr += '.' + d.getFullYear();
  datetimestr += ' ' + Number(d.getHours()).pad(2);
  datetimestr += ':' + Number(d.getMinutes()).pad(2);
  datetimestr += ':' + Number(d.getSeconds()).pad(2);
  return datetimestr;
}

function format_durationms(time) {
  if(time == '-') return time;

  var h = Number(Math.floor(time / 3600000));
  time %= 3600000;
  var m = Number(Math.floor(time / 60000));
  time %= 60000;
  var s = Number(Math.floor(time / 1000));
  var hs = Number(Math.floor((time % 1000)/100));

  return h + ':' + m.pad(2) + ':' + s.pad(2) + '.' + hs;
}

function get_rd_week(msEpoch) {
  //
  // This computes the current Rivendell Week based on the number
  // of weeks since epoch.
  //
  // Explanation:
  //  epoch was at 01.01.1970 which was a Thursday.
  //  Monday in that week is (s-from-epoch + 3*24*60*60) seconds ago.
  //  This needs to be adjusted by the timezone offset for Europe/Vienna
  //  which is of course not constant (damn you daylight savings time)
  //  Divide this by (7*24*60*60) and you get the number of
  //  weeks since the Monday in the week of epoch adjusted for timezone offsets.
  //  This week had week number 3 so add an offset of 2 and
  //  get the modulo of 4. This rounded down gives you the current week
  //  with 0 meaning Week 1. So add 1 to that number and you will get
  //  the current RD week.
  //
  var sEpoch = msEpoch / 1000 ;
  var week = Math.floor((((sEpoch + 259200)/604800) + 2) % 4) + 1;
  return week;
}

function parseLocationHref() {
  var matches = window.location.href.match(/(https?):\/\/(.*)/);
  if(matches === null) {
    return null;
  }

  var uri = {};
  uri.scheme = matches[1];
  uri.servername = '';
  uri.path = [];
  uri.query = [];
  uri.fragment = '';

  if(matches[2].indexOf('/') < 0) {
    uri.servername = parts[2];
    return uri;
  }

  var parts = matches[2].split('/');
  uri.servername = parts[0];
  uri.path = parts.slice(1);

  var tmp = uri.path[uri.path.length-1];
  if(tmp.length > 0) {
    var qidx = tmp.indexOf('?');
    var fidx = tmp.indexOf('#');

    var query = '';
    if(qidx >= 0 && fidx >= 0) {
      uri.path[uri.path.length-1] = tmp.substring(0, qidx);
      if(qidx < fidx) {
        query = tmp.substring(qidx+1, fidx);
      }
      uri.fragment = tmp.substring(fidx+1);
    } else if (qidx >= 0 && fidx < 0) {
      uri.path[uri.path.length-1] = tmp.substring(0, qidx);
      query = tmp.substring(qidx+1);
    } else if (qidx < 0 && fidx >= 0) {
      uri.path[uri.path.length-1] = tmp.substring(0, fidx);
      uri.fragment = tmp.substring(fidx+1);
    }
    if(query.length > 0) {
      uri.query = query.split('&');
    }
  }

  return uri
}

function locationHrefValue() {
  var url = parseLocationHref();
  if (url.path.length > 1) {
    return [url.path[0] + '/' + url.path[1], url.path[0], url.path[1]]
  }
  return (url.path.length > 0) ? [url.path[0], url.path[0]] : '';
}

/*
 see: https://gist.github.com/kozo002/6806421
 */
jQuery.fn.isBackgroundDark = function() {
  return this.brightness() === 'dark';
};

jQuery.fn.brightness = function() {
  var bg_color, rgba, y;
  bg_color = this.css('background-color');
  if ((bg_color != null) && bg_color.length) {
    rgba = bg_color.match(/^rgb(?:a)?\(([0-9]{1,3}),\s([0-9]{1,3}),\s([0-9]{1,3})(?:,\s)?([0-9]{1,3})?\)$/);
    if (rgba != null) {
      if (rgba[4] === '0') {
        if (this.parent().length) return this.parent().brightness();
      } else {
        y = 2.99 * rgba[1] + 5.87 * rgba[2] + 1.14 * rgba[3];
        if (y >= 1275) {
          return 'light';
        } else {
          return 'dark';
        }
      }
    }
  } else {
    if (this.parent().length) return this.parent().brightness();
  }
};

jQuery.fn.sort = function() {
  return this.pushStack(jQuery.makeArray([].sort.apply(this, arguments)));
};

function updateProgressBar($el, upload) {
  if(upload.uploadprogress.progress_step < 2) {
    var bytes_str = Number((upload.uploadprogress.current/1024)/1024).toFixed(1) + " von " +
      Number((upload.uploadprogress.total/1024)/1024).toFixed(1) + " MB";
    $el.find('.file-bytes').text(bytes_str);
    $el.find('.progress .progress-bar').css("width", upload.uploadprogress.progress + "%");
  } else if(upload.uploadprogress.progress_step < 3) {
    $el.find('.file-bytes').text('normalisiere...');
    $el.find('.progress .progress-bar').css("width", upload.uploadprogress.progress + "%");
  } else {
    $el.find('.file-bytes').text('importiere...');
    if (upload.uploadprogress.progress < 99) {
      $el.find('.progress .progress-bar').css("width", upload.uploadprogress.progress + "%");
    } else {
      $el.find('.progress .progress-bar').css("width", "100%");
      $el.find('.progress .progress-bar').addClass('progress-bar-striped').addClass('active');
    }
  }
}
