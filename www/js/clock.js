/*
 *  rhwebimport
 *
 *  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
 *  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
 *
 *  This file is part of rhwebimport.
 *
 *  rhwebimport is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhwebimport is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var Rdxport = Rdxport || {};

function Clock() {
  this.draw_callbacks = $.Callbacks('unique');

  this.last_message = { t1: 0, t2: 0, t3: 0, t4: 0, tz_offset: 3600 };
  this.clock_offset = 0;
  this.clock_rtt = 0;
  this.state = 'NEW';

  this.getRDTimeMS = function() {
    return (+new Date()) + (this.last_message.tz_offset * 1000) + this.clock_offset;
  }

  this.now = function() {
    return new Date(this.getRDTimeMS());
  }

  this.redraw = function() {
    var rdtime_ms = this.getRDTimeMS();

    var rdtime = new Date(rdtime_ms);
    var date_str = weekday_short[rdtime.getUTCDay()]  + ', ';
    date_str += rdtime.getUTCDate() + '.' + (rdtime.getUTCMonth() + 1) + '.' + rdtime.getUTCFullYear();

    var time_str = (rdtime.getUTCHours() > 9 ? ' ' : ' 0') + rdtime.getUTCHours();
    time_str += (rdtime.getUTCMinutes() > 9 ? ':' : ':0') + rdtime.getUTCMinutes();
    time_str += (rdtime.getUTCSeconds() > 9 ? ':' : ':0') + rdtime.getUTCSeconds();

    this.draw_callbacks.fireWith(window, [date_str, time_str, get_rd_week(rdtime_ms)]);
  };

  this.addCallback = function(cb) {
    this.draw_callbacks.add(cb);
  };

  this.ntp_update = function(event) {
    var t4 = (+new Date());

    var msg = JSON.parse(event.data);
    msg.t4 = t4;
    this.last_message = msg;
    this.clock_offset = ((msg.t2 - msg.t1) + (msg.t3 - msg.t4)) / 2;
    this.clock_rtt = (msg.t4 - msg.t1) - (msg.t3 - msg.t2);
//    console.log('got new ntp message from rhrdtime (rtt=' + this.clock_rtt + ' ms): new offset = ' + this.clock_offset + ' ms');
  };

  this.ntp_request = function() {
    this.sock.send(JSON.stringify({ t1: (+new Date()), t2: 0, t3: 0, t4: 0, tz_offset: 0, week: 0 }));
  };

  this.sock_onopen = function() {
//    console.log('clock websocket connection established');
    this.state = 'CONNECTED';
    this.ntp_request();
    this.interval_request = setInterval(this.ntp_request.bind(this), 2000);
  };

  this.sock_onclose = function(event) {
    if(this.state == 'STOPPED') {
      delete this.sock;
    } else {
//      console.log('clock websocket closed with code ' + event.code + ', trying reconnect...');
      clearInterval(this.interval_request);
      delete this.interval_request;
      this.sock.close();
      delete this.sock;
      setTimeout(this.connect.bind(this), 1000);
      this.state = 'RECONNECTING';
    }
  };

  this.connect = function() {
    this.sock = new WebSocket('wss://' + window.location.host + '/ntp');
    this.sock.onmessage = this.ntp_update.bind(this);
    this.sock.onopen = this.sock_onopen.bind(this);
    this.sock.onclose = this.sock_onclose.bind(this);
    this.state = 'CONNECTING';
  };

  this.start = function() {
    this.connect();
    this.interval_redraw = setInterval(this.redraw.bind(this), 200);
  };

  this.stop = function() {
    this.state = 'STOPPED';
    clearInterval(this.interval_redraw);
    delete this.interval_redraw;
    clearInterval(this.interval_request);
    delete this.interval_request;
    this.sock.close();
  };
}

function clock_add_callback(cb) {
  clock.addCallback(cb);
}

function drawClock(date, time, week) {
  $('#clock span.clock-date').text(date);
  $('#clock span.clock-time').text(time);
  var weekspan = $('#clock span.current-week').removeClass().addClass('current-week').addClass('label');
  switch(week) {
    case 1:
      weekspan.addClass('label-info').text('Woche 1');
      break;
    case 2:
      weekspan.addClass('label-warning').text('Woche 2');
      break;
    case 3:
      weekspan.addClass('label-success').text('Woche 3');
      break;
    case 4:
      weekspan.addClass('label-danger').text('Woche 4');
      break;
    default:
      weekspan.addClass('label-default').text('Fehler');
  }
}
