#!/bin/bash
# Build

cd www/js
rm -f rhwebimport.js
cat rdxport.js rdxport.rh.js utils.js clock.js importer.js auth.js shows.js jingles.js musicpools.js musicgrid.js router.js >> rhwebimport.js
yui-compressor rhwebimport.js -o rhwebimport.min.js

cd ../styles
rm -f rhwebimport.css
cat main-style.css auth.css shows.css jingles.css musicpools.css musicgrid.css >> rhwebimport.css
yui-compressor rhwebimport.css -o rhwebimport.min.css

echo "done"
