#!/usr/bin/perl
#
#  rhwebimport
#
#  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
#  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
#
#  This file is part of rhwebimport.
#
#  rhwebimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhwebimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use CGI;
use POSIX;
use XML::Quote;
use RHRD::rddb;

my $status = 'ERROR';
my $errorstring = 'unknown';
my $responsecode = 500;
my @dropboxes = ();

my $q = CGI->new;
my $username = $q->param('LOGIN_NAME');
my $token = $q->param('PASSWORD');
my $type = $q->param('TYPE');

if(!defined $username) {
  $responsecode = 400;
  $errorstring = "mandatory field LOGIN_NAME is missing"
} elsif(!defined $token) {
  $responsecode = 400;
  $errorstring = "mandatory field PASSWORD is missing"
} else {
  (my $ctx, $status, $errorstring) = RHRD::rddb::init();
  if(defined $ctx) {
    my $result;
    ($result, $status, $errorstring) = RHRD::rddb::check_token($ctx, $username, $token);
    if($result == 1) {
      $responsecode = 200;
      @dropboxes = RHRD::rddb::get_dropboxes($ctx, $username, undef, $type);
      if(!defined $dropboxes[0] && defined $dropboxes[1]) {
        $responsecode = 500;
        $status = $dropboxes[1];
        $errorstring = $dropboxes[2];
      }
    } elsif($result == 0) {
      $responsecode = 401;
    } else {
      $responsecode = 500;
    }
    RHRD::rddb::destroy($ctx);
  }
}

print "Content-type: application/xml; charset=UTF-8\n";
print "Status: $responsecode\n\n";

if($responsecode != 200) {
  print "<RDWebResult>\n";
  print "  <ResponseCode>" . xml_quote($responsecode) . "</ResponseCode>\n";
  print "  <ErrorString>" . xml_quote($errorstring) . "</ErrorString>\n";
  print "</RDWebResult>\n";
} else {
  print "<dropboxList>\n";
  for my $href (@dropboxes) {
    print "  <dropbox>\n";
    print "    <group>" . xml_quote($href->{'GROUP'}) . "</group>\n";
    print "    <group-description>" . xml_quote($href->{'GROUPDESC'}) . "</group-description>\n";
    print "    <group-low-cart>" . xml_quote($href->{'GROUPLOWCART'}) . "</group-low-cart>\n";
    print "    <group-high-cart>" . xml_quote($href->{'GROUPHIGHCART'}) . "</group-high-cart>\n";
    print "    <normalization-level>" . floor($href->{'NORMLEVEL'}/100) . "</normalization-level>\n";
    print "    <autotrim-level>" . floor($href->{'TRIMLEVEL'}/100) . "</autotrim-level>\n";
    print "    <parameters>" . xml_quote($href->{'PARAM'}) . "</parameters>\n";
    print "    <type>" . xml_quote($href->{'TYPE'}) . "</type>\n";
    if($href->{'TYPE'} eq "show") {
      my $dow = $href->{'SHOWDOW'};
      $dow = 0 unless $dow < 7;
      print "    <show-id>" . xml_quote($href->{'SHOWID'}) . "</show-id>\n";
      print "    <show-title>" . xml_quote($href->{'SHOWTITLE'}) . "</show-title>\n";
      print "    <show-log>" . xml_quote($href->{'SHOWLOG'}) . "</show-log>\n";
      print "    <show-rhythm>" . xml_quote($href->{'SHOWRHYTHM'}) . "</show-rhythm>\n";
      print "    <show-dayofweek>" . xml_quote($dow) . "</show-dayofweek>\n";
      print "    <show-starttime>" . xml_quote($href->{'SHOWSTARTTIME'}) . "</show-starttime>\n";
      print "    <show-length>" . xml_quote($href->{'SHOWLEN'}) . "</show-length>\n";
      print "    <show-type>" . xml_quote($href->{'SHOWTYPE'}) . "</show-type>\n";
    } elsif($href->{'TYPE'} eq "jingle") {
      print "    <jingle-title>" . xml_quote($href->{'JINGLETITLE'}) . "</jingle-title>\n";
    } elsif($href->{'TYPE'} eq "musicpool") {
      print "    <musicpool-title>" . xml_quote($href->{'MUSICPOOLTITLE'}) . "</musicpool-title>\n";
      print "    <musicpool-clock>" . xml_quote($href->{'MUSICPOOLCLOCK'}) . "</musicpool-clock>\n";
    }
    print "  </dropbox>\n";
  }
  print "</dropboxList>\n";
}
