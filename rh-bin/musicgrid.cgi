#!/usr/bin/perl
#
#  rhwebimport
#
#  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
#  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
#
#  This file is part of rhwebimport.
#
#  rhwebimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhwebimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use CGI;
use POSIX;
use XML::Quote;
use RHRD::rddb;

my $status = 'ERROR';
my $errorstring = 'unknown';
my $responsecode = 500;

my $q = CGI->new;
my $username = $q->param('LOGIN_NAME');
my $token = $q->param('PASSWORD');
my $cmd = $q->request_method();

sub set_clock
{
  my ($ctx, $query) = @_;

  my $dow = $query->param('DOW');
  my $hour = $query->param('HOUR');
  my $shortname = $query->param('NAME');

  if(!defined $dow) {
    return 400 ,"mandatory field DOW is missing";
  } elsif($dow < 0 || $dow > 6) {
    return 400 ,"DOW is out of range";
  } elsif(!defined $hour) {
    return 400, "mandatory field HOUR is missing";
  } elsif($hour < 0 || $hour > 23) {
    return 400 ,"HOUR is out of range";
  } elsif(!defined $shortname) {
    return 400, "mandatory field NAME is missing";
  }

  my ($result, $status, $error) = RHRD::rddb::set_musicgrid_clock($ctx, $dow, $hour, $shortname);
  if(!defined $result) {
    return 500, $status . ": " . $error;
  }

  return 200, "OK";
}

my @clocks = ();
if(!defined $username) {
  $responsecode = 400;
  $errorstring = "mandatory field LOGIN_NAME is missing";
} elsif(!defined $token) {
  $responsecode = 400;
  $errorstring = "mandatory field PASSWORD is missing";
} else {
  (my $ctx, $status, $errorstring) = RHRD::rddb::init();
  if(defined $ctx) {
    (my $authenticated, $status, $errorstring) = RHRD::rddb::check_token($ctx, $username, $token);
    my $authorized = RHRD::rddb::is_musicgrid_user($ctx, $username);
    if($authenticated == 1 && $authorized == 1) {
      if($cmd eq "GET") {
        @clocks = RHRD::rddb::get_musicgrid_clocks($ctx);
        if(!defined $clocks[0] && defined $clocks[1]) {
          ($responsecode, $errorstring) = (500, $clocks[1] . ": " . $clocks[2]);
        } else {
          ($responsecode, $errorstring) = (200, "OK");
        }
      }
      elsif($cmd eq "POST") {
        ($responsecode, $errorstring) = set_clock($ctx, $q);
      }
      else {
        $responsecode = 405;
        $errorstring = "request method '$cmd' is unknown";
      }
    } elsif($authenticated == 0) {
      $responsecode = 401;
    } elsif($authorized == 0) {
      $responsecode = 403;
      $errorstring = "user '" . $username . "' is not allowed to access the music grid";
    } else {
      $responsecode = 500;
    }
    RHRD::rddb::destroy($ctx);
  }
}

print "Content-type: application/xml; charset=UTF-8\n";
print "Status: $responsecode\n\n";

if($cmd eq "POST" || $responsecode != 200) {
  print "<RDWebResult>\n";
  print "  <ResponseCode>" . xml_quote($responsecode) . "</ResponseCode>\n";
  print "  <ErrorString>" . xml_quote($errorstring) . "</ErrorString>\n";
  print "</RDWebResult>\n";
} else {
  print "<grid>\n";
  for my $href (@clocks) {
    print "  <clock dow=\"" . xml_quote($href->{'DOW'}) . "\" hour=\"" . xml_quote($href->{'HOUR'}) . "\">\n";
    print "    <name>" . xml_quote($href->{'SHORTNAME'}) . "</name>\n";
    print "    <color>" . xml_quote($href->{'COLOR'}) . "</color>\n";
    print "    <title>" . xml_quote($href->{'TITLE'}) . "</title>\n";
    print "  </clock>\n";
  }
  print "</grid>\n";
}
