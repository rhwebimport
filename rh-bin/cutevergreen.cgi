#!/usr/bin/perl
#
#  rhwebimport
#
#  Copyright (C) 2014-2016 Christian Pointner <equinox@helsinki.at>
#  Copyright (C) 2015-2016 Peter Grassberger <petertheone@gmail.com>
#
#  This file is part of rhwebimport.
#
#  rhwebimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhwebimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with rhwebimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use CGI;
use POSIX;
use XML::Quote;
use RHRD::rddb;

my $status = 'ERROR';
my $errorstring = 'unknown';
my $responsecode = 500;

my $q = CGI->new;
my $username = $q->param('LOGIN_NAME');
my $token = $q->param('PASSWORD');
my $cmd = $q->request_method();
my $cart = $q->param('CART_NUMBER');
my $cut = $q->param('CUT_NUMBER');
my $value = $q->param('VALUE');


sub is_authorized
{
  my ($ctx, $username, $cart) = @_;

  RHRD::rddb::is_group_member($ctx, $username);

  my ($groupname, undef, undef) = RHRD::rddb::get_cart_group($ctx, $cart);
  my ($cnt, undef, undef) = RHRD::rddb::is_group_member($ctx, $groupname, $username);
  unless(defined $cnt) {
    return 0;
  }
  return (($cnt) ? 1 : 0);
}

if(!defined $username) {
  $responsecode = 400;
  $errorstring = "mandatory field LOGIN_NAME is missing";
} elsif(!defined $token) {
  $responsecode = 400;
  $errorstring = "mandatory field PASSWORD is missing";
} elsif(!defined $cart) {
  $responsecode = 400;
  $errorstring = "mandatory field CART_NUMBER is missing";
} elsif($cart < RHRD::rddb::RD_MIN_CART || $cart > RHRD::rddb::RD_MAX_CART) {
  $responsecode = 400;
  $errorstring = "CART_NUMBER is out of range";
} elsif(!defined $cut) {
  $responsecode = 400;
  $errorstring = "mandatory field CUT_NUMBER is missing";
} elsif($cut < RHRD::rddb::RD_MIN_CUT || $cut > RHRD::rddb::RD_MAX_CUT) {
  $responsecode = 400;
  $errorstring = "CUT_NUMBER is out of range";
} elsif(!defined $value) {
  $responsecode = 400;
  $errorstring = "mandatory field VALUE is missing";
} else {
  (my $ctx, $status, $errorstring) = RHRD::rddb::init();
  if(defined $ctx) {
    my ($authenticated, undef, undef) = RHRD::rddb::check_token($ctx, $username, $token);
    my ($authorized, undef, undef) = is_authorized($ctx, $username, $cart);
    if($authenticated == 1 && $authorized == 1) {
      if($cmd eq "POST") {
        my ($result, $status, $error) = RHRD::rddb::set_cut_evergreen($ctx, $cart, $cut, $value);
        if(!defined $result) {
          $responsecode = 500;
          $errorstring = $status . ": " . $error;
        } else {
          $responsecode = 200;
          $errorstring = "OK"
        }
      }
      else {
        $responsecode = 405;
        $errorstring = "request method '$cmd' is unknown";
      }
    } elsif($authenticated == 0) {
      $responsecode = 401;
    } elsif($authorized == 0) {
      $responsecode = 403;
      $errorstring = "user '" . $username . "' is not allowed to access the cart/cut";
    } else {
      $responsecode = 500;
    }
    RHRD::rddb::destroy($ctx);
  }
}

print "Content-type: application/xml; charset=UTF-8\n";
print "Status: $responsecode\n\n";

print "<RDWebResult>\n";
print "  <ResponseCode>" . xml_quote($responsecode) . "</ResponseCode>\n";
print "  <ErrorString>" . xml_quote($errorstring) . "</ErrorString>\n";
print "</RDWebResult>\n";
