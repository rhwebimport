Pflichtenheft
=============

Grundsätzliches:
----------------

Die zu erstellende Software `rhwebimport` ist eine Erweiterung zu Rivendell, der bei Radio Helsinki verwendeten Automationssoftware. Diese bietet eine Web XML-RPC Schnittstelle die benutzt werden kann um Audiodateien in das System zu importieren, zu exportieren und zu bearbeiten. Rivendell verwaltet Audioinhalte in sogenannten Carts und Cuts. Jedes Cart besteht aus einem oder mehreren Cuts. Weiters gibt es sogenannte Logs welche einfach eine Liste aus Carts sind.

Eine Sendung auf Radio Helsinki hat genau ein Log welches abgearbeitet wird wenn die Sendung laufen sollen. Jedes dieser Logs referenziert auf ein oder mehrere Carts und jedes Cart besteht aus einem oder mehreren Cuts welche die eigentliche Audioinformation enthalten. Die Ablaufsteuerung wird von Rivendell übernommen und ist nicht Teil der Software `rhwebimport`.

Zu den Aufgaben der Sendungsmachenden bei Radio Helsinki gehört es, die Carts und Cuts, die zu einer Sendung gehören, rechtzeitig mit den richtigen Inhalten zu befüllen. `rhwebimport` ist jene Applikation die von den Sendungsmachenden zu diesem Zweck verwendet wird.

Neben Sendungen gibt es noch Jingles und Musicpools. Letzere sind Pools aus Carts welche von ausgewählten Personen verwaltet werden. Rivendell generiert jeden Tag eine Playlist für das Musikprogramm welches immer dann onAir zu hören ist wenn keine vorproduzierte oder Live Sendung gespielt wird. Diese Playlist enthält zufällig aus den Pools ausgewählte Musikstücke. Ausserdem wird in regelmäßigen Abständen ein Jingle in das Musikprogramm eingstreut. Rivendell verwendet je nach Tag und Uhrzeit unterschiedliche Musicpools für die Generierung der Tagesplaylist. Die Generierung und Ablaufsteuerung der Playlist ist nicht Teil von `rhwebimport`. `rhwebimport` soll nur dazu verwendet werden, in die Carts der Pools Musik zu importieren oder zu entfernen. Die Zuweisung der Musicpools zu einer Tageszeit erfolgt in einem Stundenraster. Dazu gibt es für jeden Pool ein sogenanntes Clock. Die Zuweisung der Clocks zu den 24 Stunden eines Tages erfolgt über das sogenannte Grid. Dieses Grid enthält für jede Stunde einer Woche eine Referenz auf genau ein Clock.
Ausserdem sollen mit `rhwebimport` die aktuellen Jingles verwaltet werden (import, entfernen, temporäres deaktivieren). Anders als bei Sendungs und Musicpoolcarts, die nur ein Cut beinhalten, gibt es für die verschiedenen Jingle Gruppen je nur ein Cart mit jeweils mehreren Cuts. Das System wird zufällig eines der Cuts spielen, sobald das Jinglecart referenziert wird. Derzeit sind 2 Jingle Gruppen geplant (Allgemein und Anlassbezogen) welche zu unterschiedlichen Zeiten gespielt werden.

`rhwebimport` soll als Single-Page HTML5/CSS3 Applikation auf Basis von Twitter-Bootstrap, JQuery und dropbozone.js entwickelt werden. Zusätzlicher externer Javascript Code kann verwendet werden, muss aber mit dem Technikteam von Radio Helsinki abgesprochen sein. Insbesondere das Laden von externen Quellen ist zu vermeiden.
Die Applikation soll zumindest in folgenden Browser-Betriebsystem Konfigurationen funktionieren:

 * *Firefox* unter Linux, Windows, MacOS
 * *Chrome/Chromium* unter Linux, Windows, MacOS
 * *Internet Explorer* unter Windows
 * *Safari* unter MacOS

Bei den mobilen Platformen soll zumindest Android ab Version 4 und iOS ab Version 7 unterstützt werden.

`rhwebimport` soll unter der Lizenz AGPL in der Version 3 entwickelt werden.

Für das gesamte Interface von `rhwebimport` ist auf Barrierefreiheit zu achten.

Weiters soll für das Design des Interfaces die Ideen und Erfahrungen von Sendungsmachenden und Angestellten von Radio Helsinki einfließen. Es wird einen oder mehrere Termine mit diesen Personen geben, um dies zu ermöglichen. Die Organisation dieser Termine übernimmt Radio Helsinki.

Radio Helsinki verpflichtet sich, die Entwicklung durch zur Verfügungstellung von Testinfrastruktur zu unterstützen. Radio Helsinki wird für die Entwicklung Accounts am Testsystem bzw. zu anderen zur Entwicklung benötigten Systeme zur Vefügung stellen und kümmert sich um die Wartung dieser Systeme.
Ausserdem sollen etwaige server-seitige Schnittstellen in Kooperation erarbeitet werden. Dies gilt insbesondere für Schnittstellen, die `rhwebimport` benötigt, für die es aber keine Entsprechung in der von Rivendell zur Verfügung gestellten API gibt. Die dafür benötigten server-seitigen Komponenten werden von Radio Helsinki entwickelt.
Weiters stellt Radio Helsinki ein Git Repository zur Quellcodeverwaltung zur Verfügung.


Basisfunktionalität:
--------------------

### Sendungs Imports/Verwaltung
Diese Komponente ist bereits weitgehend fertiggestellt und bedarf nur noch ein paar kleinerer Anpassungen. Insbesondere für die Usability sind Vorschläge zu erarbeiten, damit auch weniger erfahrene Sendungsmachende das System verwenden können. Auch ist zu überprüfen, ob das bestehende Layout auch als mobile Anwendung funktional bleibt. Dafür ist mit einer Minimalauflösung von 320x480 zu rechnen. Allerdings sind für diese kleinste Auflösung Einschränkungen erlaubt. Die uneingeschränkte Funktionalität ist erst ab einer Auflösung von 360x640 zu gewährleisten. Dies gilt auch für alle anderen Komponenten von `rhwebimport`.

### Jingles Import/Verwaltung
Für die Verwaltung der Jingles soll es, ähnlich wie beim Import für Sendungen, ein Drop Down Menü für die Auswahl der Jingle Gruppe geben. Jede Gruppe entspricht genau einem Cart. Die zu importierenden Dateien werden als Cuts innerhalb dieses Carts gespeichert. Im Gegensatz zum Import für Sendungen spielt die Reihenfolge der Cuts keine Rolle. Deshalb kann und soll zur Verbesserung der Übersichtlichkeit eine Umsortierung (zb. alphabetisch nach Titel) stattfinden. Importierte Jingles sollen gelöscht oder deaktiviert werden können. Löschen soll durch Entfernen des Cuts implementiert werden. Die Deaktivierung eines Jingles soll durch Verschieben des Cuts in ein anderes Cart erfolgen.
Die dafür benötigten API Funktionen sollen gemeinsam mit dem Technikteam von Radio Helsinki besprochen und anschließend von Radio Helsinki entwickelt werden.

### Musicpools Import/Verwaltung
Für jedes Musicpool gibt es einen Cart Bereich. Innerhalb dieses Bereichs sollen Audiodateien importiert bzw entfernt werden können. Jede Datei soll in einem eigenen Cart gespeichert sein. D.h. Jedes Cart besitzt genau ein Cut oder das Cart ist nicht vorhanden. Wie bei den Jingles gilt, dass die Reihenfolge der Carts keine Rolle spielt. Eine sinnvolle Umsortierung in der Anzeige ist deshalb möglich und soll implementiert werden. Weiters soll es möglich sein, Filter auf Interpret/Titel/Album anzuwenden.
Die dafür benötigten API Funktionen sollen gemeinsam mit dem Technikteam von Radio Helsinki besprochen und anschließend von Radio Helsinki entwickelt werden.


erweiterte Funktionen:
----------------------

### Musicpool Clock Verwaltung
Es ist ein Interface zu entwickeln, mit dessen Hilfe die Zuweisung der Musikpool Clocks zum Grid verändert werden kann. Das Interface muss dabei sicherstellen, dass jeder Stundenslot immer auf eine gültiges Clock verweist und keine Löcher entstehen können. Vereinfacht gesagt: die Referenzen im Grid dürfen nur überschrieben, aber nicht gelöscht werden können.

### erweiterte Import-Quellen
Es soll möglich sein Dateien direkt aus verschiednen exteren Quellen zu importieren. Zu diesem Zweck gibt es serverseitig die Komponente `rhimportd`. Diese wird über eine Websocket-API gesteuert und kümmert sich um den tatsächlichen Import. `rhwebimport` initiiert den Import und visualisiert den Fortschritt. Ausserdem soll es möglich sein den Fortschritt etwaiger laufender Import-Prozesse nach einem erneuten Einloggen (zb. nach einem Verbindungsabbruch) weiter anzuzeigen. `rhimportd` bietet alle dafür notwendigen API Funktionen. Sollten zu diese Zweck Änderungen an der API notwendig sein sollen diese gemeinsam mit dem Radio Helsinki Technik-Team erarbeitet werden. Die Änderungen an `rhimportd` werden von Radio Helsinki erledigt.
Folgende externe Quellen sollen unterstützt werden:

 - archiv://YYYY/MM/DD/hh/mm - import aus dem Sendungsarchiv von Radio Helsinki (derzeit nur für volle Stunden, dh. mm muss immer 00 sein)
 - http://, https://, ftp://, ftps:// - import von der entsprechenden URL

### zustätliche Importquelle: `public://<username>/`
Es soll möglich sein Dateien aus dem 'public' Verzeichnis des Benutzers zu importieren. Dazu ist es notwendig für die Auswahl der Datei eine Übersichtseite zu erstellen aus der dann die Datei ausgewählt wird. Die dazu notwendigen Änderungen an der API von `rhimportd` sind gemeinsam mit dem Radio Helsinki Technik-Team zu erarbeiten. Die Änderungen an `rhimportd` werden von Radio Helsinki vorgenommen.

### zustätliche Importquelle: `cba://<cba-id>`
Es soll möglich sein Sendungsübernahmen direkt aus der CBA, dem Cultural Broadcasting Archiv (cba.fro.at) zu importieren. Etwaige dazu notwendigen Änderungen an der API von `rhimportd` sind gemeinsam mit dem Radio Helsinki Technik-Team zu erarbeiten. Die Änderungen an `rhimportd` werden von Radio Helsinki vorgenommen. Zusätzlich soll es möglich sein den Sendungstitel und den Beschreibungstext aus der CBA zu übernehmen und in die Programmverwaltung von Radio Helsinki zu übertragen. Die Auswahl des Timeslots innerhalb der Programmverwaltung soll über eine Ansicht in `rhwebimport` erfolgen. Etwaige API Änderungen an `rhimportd` oder der Programmverwaltung sind gemeinsam mit dem Radio Helsinki Technik-Team zu erarbeiten. Die Änderungen an `rhimportd` sowie der Programmverwaltung werden von Radio Helsinki vorgenommen.

### Sendungcarts verschieben (innerhalb eines Sendungs Logs)
Die Audioinhalte, die in den Carts einer Sendung gespeichert sind, sollen verschiebbar werden. Dafür ist ein leicht zu verstehendes Userinterface zu erarbeiten und zu implementieren.

### Sendungcarts verschieben (zwischen 2 Sendungen)
Audioinhalte sollen von einem Cart einer Sendung in ein Cart einer anderen Sendung verschoben oder kopiert werden können. Dafür ist ein leicht zu verstehendes Userinterface zu erarbeiten und zu implementieren.

### Musicpool Import aus Playlist
Der Import für Musikpools soll Playlists unterstützen. Das heißt, sollte eine Datei, die in die Dropzone gezogen wird, eine Playlist sein, sind anstelle der Playlist die darin referenzierten Dateien zu importieren. Diese Funktion soll mindestens die Formate M3U und PLS unterstützen.

### Musicpool Syncronisation mit Playlist
Eine einmal für den Import verwendete Playlist soll nach einer Veränderung zu einem späteren Zeitpunkt mit dem in Rivendell gespeicherten Dateien abgeglichen werden können. Das Interface soll dann folgende Funktionen anbieten: Hinzufügen neuer Dateien, Löschen aller Carts, die nicht mehr in der Playlist sind und eine Kombination daraus. Es ist eine Lösung zu erarbeiten, die die Erkennung einer bereits importierten Datei ermöglicht. Die dafür herangezogenen Kennwerte müssen innerhalb des Cart verspeichert werden - zu diesem Zweck gibt es bei jedem Cart ein Feld 'user-defined'.
